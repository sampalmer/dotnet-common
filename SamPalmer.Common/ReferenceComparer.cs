﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamPalmer.Common.Portable
{
	public class ReferenceComparer : Comparer<object>
	{
		public override int Compare(object x, object y)
		{
			var xIsNull = x == null;
			var yIsNull = y == null;

			if (xIsNull == yIsNull)
				return 0;
			else
				return xIsNull ? -1 : 1;
		}
	}
}
