﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common
{
	public static class EnumerableUtilities
	{
		public static IEnumerable<TResult> Select<TResult>(Func<TResult> function, int numberOfTimes)
		{
			if (function == null)
				throw new ArgumentNullException(ReflectionUtilities.GetName(() => function));

			return Enumerable.Range(0, numberOfTimes).Select(index => function());
		}

		public static TimeSpan Sum(this IEnumerable<TimeSpan> times)
		{
			var totalTicks = times.Sum(time => time.Ticks);
			return TimeSpan.FromTicks(totalTicks);
		}

		public static TimeSpan Average(this IEnumerable<TimeSpan> times)
		{
			var totalTime = times.Sum();
			var totalTicks = totalTime.Ticks;
			var fractionalAverageTicks = (decimal)totalTicks / times.LongCount();
			var roundedAverageTicks = (long)Math.Round(fractionalAverageTicks);
			return TimeSpan.FromTicks(roundedAverageTicks);
		}

		public static bool Multiple<T>(this IEnumerable<T> enumerable)
		{
			var subsequentItems = enumerable.Where(IsNotFirstItem);
			return subsequentItems.Any();
		}
		
		private static bool IsNotFirstItem<T>(T item, int index)
		{
			return index > 0;
		}
	}
}
