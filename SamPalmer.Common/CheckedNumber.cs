using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common.Portable
{
	/// <summary>Provides checked arithmetic and delays exception throwing to the point at which the underlying value is retrieved.</summary>
	/// <typeparam name="T">The underlying type that stores the value.</typeparam>
	public class CheckedNumber<T>
		where T : IComparable<T>, IEquatable<T>
	{
		private readonly T value;
		private readonly Exception exception;

		/// <summary>
		/// Retrieves the value or throws an exception.
		/// </summary>
		/// <exception cref="System.ArithmeticException">If an <see cref="System.ArithmeticException" /> arithmetic exception occurred during an operation.</exception>
		public T Value
		{
			get
			{
				if (exception != null)
					throw new ArithmeticException("At least one arithmetic exception occurred during an operation. See the inner exception for details.", exception);

				return value;
			}
		}

		/// <summary>
		/// Returns false if an exception occurred during an operation.
		/// </summary>
		public bool HasValue
		{
			get
			{
				return exception == null;
			}
		}

		public CheckedNumber(T value)
		{
			this.value = value;
		}

		private CheckedNumber(Exception exception)
		{
			this.exception = exception;
		}

		private CheckedNumber(IEnumerable<Exception> flattenedExceptions)
		{
			if (flattenedExceptions.Multiple())
				exception = new AggregateException(flattenedExceptions);
			else
				exception = flattenedExceptions.First();
		}

		public static CheckedNumber<T> operator -(CheckedNumber<T> self)
		{
			return DoCheckedOperation(self, a => -a);
		}

		public static CheckedNumber<T> operator +(CheckedNumber<T> augend, CheckedNumber<T> addend)
		{
			return DoCheckedOperation(augend, addend, (a, b) => a + b);
		}

		public static CheckedNumber<T> operator -(CheckedNumber<T> minuend, CheckedNumber<T> subtrahend)
		{
			return DoCheckedOperation(minuend, subtrahend, (a, b) => a - b);
		}

		public static CheckedNumber<T> operator *(CheckedNumber<T> multiplicand, CheckedNumber<T> multiplier)
		{
			return DoCheckedOperation(multiplicand, multiplier, (a, b) => a * b);
		}

		public static CheckedNumber<T> operator /(CheckedNumber<T> dividend, CheckedNumber<T> divisor)
		{
			return DoCheckedOperation(dividend, divisor, (a, b) => a / b);
		}

		public static CheckedNumber<T> operator %(CheckedNumber<T> dividend, CheckedNumber<T> divisor)
		{
			return DoCheckedOperation(dividend, divisor, (a, b) => a % b);
		}

		private static CheckedNumber<T> DoCheckedOperation(CheckedNumber<T> a, Func<dynamic, T> operation)
		{
			return DoCheckedOperation(() => operation(a.value), a);
		}

		private static CheckedNumber<T> DoCheckedOperation(CheckedNumber<T> a, CheckedNumber<T> b, Func<dynamic, dynamic, T> operation)
		{
			return DoCheckedOperation(() => operation(a.value, b.value), a, b);
		}

		private static CheckedNumber<T> DoCheckedOperation(Func<T> parameterlessOperation, params CheckedNumber<T>[] operands)
		{
			var flattenedExceptions = AggregateAndFlattenExceptions(operands);
			if (flattenedExceptions.Any())
				return new CheckedNumber<T>(flattenedExceptions);
			else
				return DoCheckedOperation(parameterlessOperation);
		}

		private static IEnumerable<Exception> AggregateAndFlattenExceptions(CheckedNumber<T>[] checkedNumbers)
		{
			var flattenedExceptions = checkedNumbers.SelectMany(GetExceptions);
			return flattenedExceptions;
		}

		private static IEnumerable<Exception> GetExceptions(CheckedNumber<T> checkedNumber)
		{
			if (HasException(checkedNumber))
			{
				var exception = checkedNumber.exception;
				var aggregateException = exception as AggregateException;
				var isAggregateException = aggregateException != null;

				if (isAggregateException)
					return aggregateException.InnerExceptions;
				else
					return new[] { exception };
			}
			else
				return new Exception[] {};
		}

		public static bool HasException(CheckedNumber<T> checkedNumber)
		{
			return !checkedNumber.HasValue;
		}

		private static CheckedNumber<T> DoCheckedOperation(Func<T> operation)
		{
			try
			{
				checked
				{
					var result = operation();
					return new CheckedNumber<T>(result);
				}
			}
			catch (ArithmeticException exception)
			{
				return new CheckedNumber<T>(exception);
			}
		}
	}
}
