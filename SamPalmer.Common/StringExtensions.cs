﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;

namespace SamPalmer.Common
{
	public static class StringExtensions
	{
		public static bool Contains(this string text, string potentialSubstring, StringComparison comparisonType)
		{
			Contract.Requires(!String.IsNullOrEmpty(text), "text is null or empty.");
			Contract.Requires(!String.IsNullOrEmpty(potentialSubstring), "potentialSubstring is null or empty.");

			var substringIndex = text.IndexOf(potentialSubstring, comparisonType);
			return substringIndex != -1;
		}

		public static bool ContainsAny(this string text, StringComparison comparisonType, params string[] potentialSubstrings)
		{
			return potentialSubstrings.Any(potentialSubstring => text.Contains(potentialSubstring, comparisonType));
		}

		public static string After(this string text, string substring)
		{
			var substringIndex = text.IndexOf(substring, StringComparison.Ordinal);
			var indexAfterSubstring = substringIndex + substring.Length;
			return text.Substring(indexAfterSubstring);
		}
	}
}
