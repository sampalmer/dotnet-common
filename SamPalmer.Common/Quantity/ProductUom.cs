﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamPalmer.Common.Portable
{
	public class ProductUom<TMultiplicandUom, TMultiplierUom> : UnitOfMeasurement
		where TMultiplicandUom : UnitOfMeasurement, new()
		where TMultiplierUom : UnitOfMeasurement, new()
	{
		public ProductUom()
			: base(true, FormatSymbol(false), FormatSymbol(true))
		{
		}

		private static string FormatSymbol(bool plural)
		{
			var multiplicandSymbol = new TMultiplicandUom();
			var multiplierUom = new TMultiplierUom();
			var multiplierSymbol = plural ? multiplierUom.Plural : multiplierUom.Symbol;

			return String.Format("{0}·{1}", multiplicandSymbol, multiplierSymbol);
		}
	}
}
