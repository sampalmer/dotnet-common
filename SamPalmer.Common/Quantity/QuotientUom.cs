﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamPalmer.Common.Portable
{
	public class QuotientUom<TDividendUom, TDivisorUom> : UnitOfMeasurement
		where TDividendUom : UnitOfMeasurement, new()
		where TDivisorUom : UnitOfMeasurement, new()
	{
		public QuotientUom()
			: base(true, FormatSymbol(false), FormatSymbol(true))
		{
		}

		private static string FormatSymbol(bool plural)
		{
			var numeratorUom = new TDividendUom();
			var numeratorSymbol = plural ? numeratorUom.Plural : numeratorUom.Symbol;
			var denominatorSymbol = new TDivisorUom().Symbol;

			return String.Format("{0}/{1}", numeratorSymbol, denominatorSymbol);
		}
	}
}
