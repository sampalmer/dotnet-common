﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common.Portable.Quantity
{
	public class Proportion<TUom>
		where TUom : UnitOfMeasurement, new()
	{
		private readonly Quantity<TUom> amount;
		private readonly Quantity<TUom> total;

		public Proportion(Quantity<TUom> amount, Quantity<TUom> total)
		{
			this.amount = amount;
			this.total = total;
		}

		public string ToString(string amountFormat, string totalFormat)
		{
			if (amount.InternalAmount.HasValue && total.InternalAmount.HasValue)
			{
				var formattedAmount = amount.FormatAmount(amountFormat);
				var formattedTotal = total.FormatAmount(totalFormat);

				return String.Format("{0}/{1} {2}", formattedAmount, formattedTotal, amount.UnitOfMeasurement.Plural);
			}
			else
				return amount.ToString();
		}

		public override string ToString()
		{
			return ToString(null, null);
		}
	}
}
