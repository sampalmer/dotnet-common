﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamPalmer.Common.Portable
{
	public abstract class UnitOfMeasurement
	{
		public bool LeadingSpace { get; private set; }
		public string Symbol { get; private set; }
		public string Plural { get; private set; }

		public UnitOfMeasurement(bool leadingSpace, string symbol)
			: this(leadingSpace, symbol, symbol)
		{
		}

		public UnitOfMeasurement(bool leadingSpace, string symbol, string plural)
		{
			LeadingSpace = leadingSpace;
			Symbol = symbol;
			Plural = plural;
		}
	}
}
