﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamPalmer.Common.Portable.Quantity
{
	public class DimensionlessUom : UnitOfMeasurement
	{
		public DimensionlessUom()
			: base(false, String.Empty)
		{
		}
	}
}
