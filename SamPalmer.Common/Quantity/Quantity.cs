﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SamPalmer.Common.Portable.Quantity;

namespace SamPalmer.Common.Portable
{
	/*
	 * TOOD:
	 * Alter UsageStatistics to use Quantities.
	 * Support auto-conversion to different units of _data_.
	 * Support division.
	 * Create a fraction class to represent usage/total.
	 * Support string formatting for MainPage.xaml.
	 * Support some sort of custom string formatting for rounding to 2 decimal places.
	 */
	public class Quantity<TUom>
		where TUom : UnitOfMeasurement, new()
	{
		internal CheckedNumber<decimal> InternalAmount { get; private set; }
		
		public decimal Amount
		{
			get
			{
				return InternalAmount.Value;
			}
		}

		public TUom UnitOfMeasurement
		{
			get
			{
				return new TUom();
			}
		}

		public Quantity(decimal amount)
			: this(new CheckedNumber<decimal>(amount))
		{
		}

		internal Quantity(CheckedNumber<decimal> amount)
		{
			InternalAmount = amount;
		}

		public static Quantity<TUom> operator -(Quantity<TUom> self)
		{
			return new Quantity<TUom>(-self.InternalAmount);
		}

		public static Quantity<TUom> operator +(Quantity<TUom> augend, Quantity<TUom> addend)
		{
			return MakeQuantity(augend, addend, (a, b) => a + b);
		}

		public static Quantity<TUom> operator -(Quantity<TUom> minuend, Quantity<TUom> subtrahend)
		{
			return MakeQuantity(minuend, subtrahend, (a, b) => a - b);
		}

		public static Quantity<TUom> operator *(Quantity<TUom> multiplicand, Quantity<DimensionlessUom> multiplier)
		{
			return MakeQuantity(multiplicand, multiplier, (a, b) => a * b);
		}

		public static Quantity<TUom> operator *(Quantity<TUom> multiplicand, decimal multiplier)
		{
			return multiplicand * new Quantity<DimensionlessUom>(multiplier);
		}

		public static Quantity<TUom> operator /(Quantity<TUom> dividend, Quantity<DimensionlessUom> divisor)
		{
			return MakeQuantity(dividend, divisor, (a, b) => a / b);
		}

		public static Quantity<TUom> operator /(Quantity<TUom> dividend, decimal divisor)
		{
			return dividend / new Quantity<DimensionlessUom>(divisor);
		}

		private static Quantity<TUom> MakeQuantity<TOperandBUom>(Quantity<TUom> a, Quantity<TOperandBUom> b, Func<CheckedNumber<decimal>, CheckedNumber<decimal>, CheckedNumber<decimal>> operation)
			where TOperandBUom : UnitOfMeasurement, new()
		{
			var resultAmount = operation(a.InternalAmount, b.InternalAmount);
			return new Quantity<TUom>(resultAmount);
		}

		public string ToString(string format)
		{
			if (InternalAmount.HasValue)
			{
				var separator = UnitOfMeasurement.LeadingSpace ? " " : String.Empty;
				var symbol = Amount == 1 ? UnitOfMeasurement.Symbol : UnitOfMeasurement.Plural;

				return String.Format("{0}{1}{2}", FormatAmount(format), separator, symbol);
			}
			else
				return "Undefined";
		}

		internal string FormatAmount(string format)
		{
			return format == null ? Amount.ToString() : Amount.ToString(format);
		}

		public override string ToString()
		{
			return ToString(null);
		}
	}

	public static class QuantityExtensions
	{
		public static Quantity<ProductUom<TMultiplicandUom, TMultiplierUom>> Times<TMultiplicandUom, TMultiplierUom>(this Quantity<TMultiplicandUom> multiplicand, Quantity<TMultiplierUom> multiplier)
			where TMultiplicandUom : UnitOfMeasurement, new()
			where TMultiplierUom : UnitOfMeasurement, new()
		{
			return Multiply<TMultiplicandUom, TMultiplierUom, ProductUom<TMultiplicandUom, TMultiplierUom>>(multiplicand, multiplier);
		}

		public static Quantity<TMultiplicandUom> DividedBy<TMultiplicandUom, TDivisorUom>(this Quantity<ProductUom<TMultiplicandUom, TDivisorUom>> dividend, Quantity<TDivisorUom> divisor)
			where TMultiplicandUom : UnitOfMeasurement, new()
			where TDivisorUom : UnitOfMeasurement, new()
		{
			return Divide<ProductUom<TMultiplicandUom, TDivisorUom>, TDivisorUom, TMultiplicandUom>(dividend, divisor);
		}

		public static Quantity<QuotientUom<TDividendUom, TDivisorUom>> DividedBy<TDividendUom, TDivisorUom>(this Quantity<TDividendUom> dividend, Quantity<TDivisorUom> divisor)
			where TDivisorUom : UnitOfMeasurement, new()
			where TDividendUom : UnitOfMeasurement, new()
		{
			return Divide<TDividendUom, TDivisorUom, QuotientUom<TDividendUom, TDivisorUom>>(dividend, divisor);
		}

		public static Quantity<TDividendUom> Times<TDividendUom, TMultiplierUom>(this Quantity<QuotientUom<TDividendUom, TMultiplierUom>> multiplicand, Quantity<TMultiplierUom> multiplier)
			where TDividendUom : UnitOfMeasurement, new()
			where TMultiplierUom : UnitOfMeasurement, new()
		{
			return Multiply<QuotientUom<TDividendUom, TMultiplierUom>, TMultiplierUom, TDividendUom>(multiplicand, multiplier);
		}

		public static Quantity<TDividendUom> Times<TMultiplicandUom, TMultiplierUom, TDividendUom>(this Quantity<TMultiplicandUom> multiplicand, Quantity<QuotientUom<TDividendUom, TMultiplicandUom>> multiplier)
			where TMultiplicandUom : UnitOfMeasurement, new()
			where TDividendUom : UnitOfMeasurement, new()
		{
			return Multiply<TMultiplicandUom, QuotientUom<TDividendUom, TMultiplicandUom>, TDividendUom>(multiplicand, multiplier);
		}
		
		private static Quantity<TProductUom> Multiply<TMultiplicandUom, TMultiplierUom, TProductUom>(Quantity<TMultiplicandUom> multiplicand, Quantity<TMultiplierUom> multiplier)
			where TMultiplicandUom : UnitOfMeasurement, new()
			where TMultiplierUom : UnitOfMeasurement, new()
			where TProductUom : UnitOfMeasurement, new()
		{
			return new Quantity<TProductUom>(multiplicand.InternalAmount * multiplier.InternalAmount);
		}

		private static Quantity<TQuotient> Divide<TDividend, TDivisor, TQuotient>(Quantity<TDividend> dividend, Quantity<TDivisor> divisor)
			where TDividend : UnitOfMeasurement, new()
			where TDivisor : UnitOfMeasurement, new()
			where TQuotient : UnitOfMeasurement, new()
		{
			return new Quantity<TQuotient>(dividend.InternalAmount / divisor.InternalAmount);
		}
	}
}
