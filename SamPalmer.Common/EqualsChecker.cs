﻿namespace SamPalmer.Common
{
	public class EqualsChecker
	{
		public static bool ReflectionCheck(object first, object second)
		{
			if (object.ReferenceEquals(first, null) || object.ReferenceEquals(second, null))
				return object.ReferenceEquals(first, second);

			var aType = first.GetType();
			var bType = second.GetType();
			if (aType != bType)
				return false;

			var fields = ReflectionUtilities.GetInstanceFields(aType);
			foreach (var field in fields)
			{
				var aValue = field.GetValue(first);
				var bValue = field.GetValue(second);

				if (!FieldsAreEqual(aValue, bValue))
					return false;
			}

			return true;
		}

		private static bool FieldsAreEqual(object a, object b)
		{
			if (object.ReferenceEquals(a, null) || object.ReferenceEquals(b, null))
			{
				if (!object.ReferenceEquals(a, b))
					return false;
			}
			else if (!a.Equals(b))
				return false;

			return true;
		}
	}
}