﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common.Portable
{
	public class CompositeComparer<TComparand> : Comparer<TComparand>, IEnumerable<Comparison<TComparand>>
	{
		private readonly List<Comparison<TComparand>> comparisons;

		public CompositeComparer()
		{
			comparisons = new List<Comparison<TComparand>>();
		}

		public void Add(Comparison<TComparand> comparison)
		{
			comparisons.Add(comparison);
		}

		public void Add<TSubcomparand>(Func<TComparand, TSubcomparand> getSubcomparand, Comparison<TSubcomparand> subcomparison)
		{
			Comparison<TComparand> comparison = (a, b) => subcomparison(getSubcomparand(a), getSubcomparand(b));
			Add(comparison);
		}

		public void Add<TSubcomparand>(Func<TComparand, TSubcomparand> getSubcomparand)
			where TSubcomparand : IComparable<TSubcomparand>
		{
			Comparison<TSubcomparand> subcomparison = (a, b) => a.CompareTo(b);
			Add(getSubcomparand, subcomparison);
		}

		public override int Compare(TComparand a, TComparand b)
		{
			if (a == null || b == null)
				return new ReferenceComparer().Compare(a, b);

			foreach (var comparison in comparisons)
			{
				var result = comparison(a, b);
				if (result != 0)
					return result;
			}

			return 0;
		}

		public IEnumerator<Comparison<TComparand>> GetEnumerator()
		{
			return comparisons.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
