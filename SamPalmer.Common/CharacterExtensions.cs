﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common
{
	public static class CharacterExtensions
	{
		public static int ToNumber(this char character)
		{
			if (character < '0' || character > '9')
				throw new ArgumentOutOfRangeException(ReflectionUtilities.GetName(() => character), "Only number characters are supported.");

			return character - '0';
		}
	}
}
