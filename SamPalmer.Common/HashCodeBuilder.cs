﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace SamPalmer.Common
{
	public class HashCodeBuilder
	{
		private int seedPrime;
		private int multiplierPrime;

		public HashCodeBuilder(int seedPrime, int multiplierPrime)
		{
			this.seedPrime = seedPrime;
			this.multiplierPrime = multiplierPrime;
		}

		public int ReflectionBuild(object instance)
		{
			var fields = GetInstanceFieldValues(instance);
			return Build(fields);
		}

		public int Build(IEnumerable<object> fieldValues)
		{
			Contract.Requires(fieldValues != null, "fieldValues is null.");

			var hashCode = seedPrime;

			foreach (var field in fieldValues)
				hashCode = unchecked(multiplierPrime * hashCode + field.GetHashCode());

			return hashCode;
		}

		private IEnumerable<object> GetInstanceFieldValues(object instance)
		{
			var type = instance.GetType();
			var fields = ReflectionUtilities.GetInstanceFields(type);

			foreach (var field in fields)
				yield return field.GetValue(instance);
		}
	}
}