﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common
{
	public static class ObjectExtensions
	{
		public static bool IsDefaultForType<T>(this T value)
		{
			var equalityComparer = EqualityComparer<T>.Default;
			var defaultValueForType = default(T);
			return equalityComparer.Equals(value, defaultValueForType);
		}

		public static TResult SafeNavigate<TObject, TResult>(this TObject source, Func<TObject, TResult> useMember)
			where TResult : class
		{
			if (source == null)
				return null;
			else
				return useMember(source);
		}
	}
}
