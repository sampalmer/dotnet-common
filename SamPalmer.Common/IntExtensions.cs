﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common.Portable
{
	public static class IntExtensions
	{
		public static void Times(this int count, Action action)
		{
			foreach (var index in Enumerable.Range(0, count))
				action();
		}
	}
}
