﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common.Portable
{
	public class Win32Exception : Exception
	{
		public Win32Exception(uint errorCode)
			: base(new Win32Error(errorCode).GetMessage())
		{
			ErrorCode = errorCode;
		}

		public uint ErrorCode { get; private set; }
	}
}
