﻿using System;
using System.Diagnostics.Contracts;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace SamPalmer.Common
{
	public static class ReflectionUtilities
	{
		public static IEnumerable<FieldInfo> GetInstanceFields(Type type)
		{
			return type.GetTypeInfo().DeclaredFields.Where(field => !field.IsStatic);
		}

		public static string GetName<T>(Expression<Func<T>> getEntity)
		{
			return GetNameFromLambda(getEntity);
		}

		public static string GetName<TSource, TMember>(Expression<Func<TSource, TMember>> getEntity)
		{
			return GetNameFromLambda(getEntity);
		}

		public static void AssertNotNull(params Expression<Func<object>>[] getParameters)
		{
			foreach (var getParameter in getParameters)
			{
				var value = getParameter.Compile()();

				if (value == null)
				{
					var name = GetName(getParameter);
					throw new ArgumentNullException(name);
				}
			}
		}

		private static string GetNameFromLambda(LambdaExpression lambda)
		{
			if (lambda == null)
				throw new ArgumentNullException(GetName(() => lambda));

			var body = lambda.Body;
			var unaryExpression = body as UnaryExpression;
			if (unaryExpression != null)
				if (unaryExpression.NodeType == ExpressionType.ArrayLength)
					return "Length";
				else
					body = unaryExpression.Operand;

			var memberExpression = body as MemberExpression;
			if (memberExpression == null)
				throw new ArgumentException("No variable/property access expression was found.", GetName(() => lambda));

			return memberExpression.Member.Name;
		}
	}
}
