﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SamPalmer.Common.Portable
{
	public class Win32Error
	{
		private readonly uint errorCode;

		// the version, the sample is built upon:
		[DllImport("Kernel32.dll", SetLastError = true)]
		private static extern uint FormatMessage(uint dwFlags, IntPtr lpSource,
		   uint dwMessageId, uint dwLanguageId, ref IntPtr lpBuffer,
		   uint nSize, IntPtr pArguments);

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern IntPtr LocalFree(IntPtr hMem);

		public string GetMessage()
		{
			// from header files
			const uint FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
			const uint FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
			const uint FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;

			var buffer = IntPtr.Zero;
			var dwChars = FormatMessage(
							FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
							IntPtr.Zero,
							errorCode,
							0, // Default language
							ref buffer,
							0,
							IntPtr.Zero);

			if (dwChars == 0)
				throw new Exception("Win32 Error: " + Marshal.GetLastWin32Error());

			try
			{
				return Marshal.PtrToStringAnsi(buffer);
			}
			finally
			{
				LocalFree(buffer);
			}
		}

		public Win32Error(uint errorCode)
		{
			this.errorCode = errorCode;
		}
	}
}
