﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SamPalmer.Common;
using System.Linq.Expressions;
using FluentAssertions;

namespace SamPalmer.Common.Tests
{
	[TestClass]
	public class GetNameTests
	{
		private const string memberVariableName = "aMemberVariable";
		private const string propertyName = "AProperty";

		private int aMemberVariable = 0;
		private static int aStaticMemberVariable = 0;
		private const int aConst = 0;

		private int AProperty { get; set; }

		[TestMethod]
		public void ShouldWorkForLocalVariables()
		{
			int aLocalVariable = 0;
			Test(() => aLocalVariable, "aLocalVariable");
		}

		[TestMethod]
		public void ShouldWorkForMemberFields()
		{
			Test(() => aMemberVariable, memberVariableName);
		}

		[TestMethod]
		public void ShouldWorkForStaticMemberVariables()
		{
			Test(() => aStaticMemberVariable, "aStaticMemberVariable");
		}

		[TestMethod]
		public void ShouldNotWorkForConstants()
		{
			AssertionExtensions.ShouldThrow<ArgumentException>(() => ReflectionUtilities.GetName(() => aConst));
			//Because constants are inlined at compile-time.
		}

		[TestMethod]
		public void ShouldWorkForProperties()
		{
			Test(() => AProperty, propertyName);
		}

		[TestMethod]
		public void ShouldWorkForExplicitProperties()
		{
			Test(() => this.AProperty, propertyName);
		}

		[TestMethod]
		public void ShouldWorkForExplicitMemberVariables()
		{
			Test(() => this.aMemberVariable, memberVariableName);
		}

		[TestMethod]
		public void ShouldWorkForParameters()
		{
			TestWithParameter(null);
		}

		[TestMethod]
		public void ShouldWorkForArrayLength() //Because this is an edge case
		{
			var anArray = new object[] { };
			Test(() => anArray.Length, "Length");
		}

		[TestMethod]
		public void ShouldWorkForStructs()
		{
			var aStruct = new DateTime();
			Test(() => aStruct, "aStruct");
		}

		[TestMethod]
		public void ShouldWorkForPrimitiveValueTypes()
		{
			var aCharacter = 'c';
			Test(() => aCharacter, "aCharacter");
		}

		[TestMethod]
		public void ShouldWorkForBoxedPrimitiveTypes()
		{
			Int32 aBoxedPrimitiveType = 25;
			Test(() => aBoxedPrimitiveType, "aBoxedPrimitiveType");
		}

		[TestMethod]
		public void ShouldWorkForReferenceTypes()
		{
			var aReferenceType = new GetNameTests();
			Test(() => aReferenceType, "aReferenceType");
		}

		[TestMethod]
		public void ShouldWorkWhenProvidingParentObject()
		{
			Assert.AreEqual("Millisecond", ReflectionUtilities.GetName<DateTime, int>(a => a.Millisecond));
		}

		[TestMethod]
		public void ShouldWorkWhenMemberCastToObject()
		{
			int x = 0;
			Test<object>(() => x, "x");
		}

		[TestMethod]
		public void ShouldWorkForSubproperty()
		{
			var x = new DateTime();
			Test(() => x.TimeOfDay.Days, "Days");
		}

		private static void Test<T>(Expression<Func<T>> getEntityExpression, string expectedName)
		{
			var result = ReflectionUtilities.GetName(getEntityExpression);
			Assert.AreEqual(expectedName, result);
		}

		private static void TestWithParameter(object aParameter)
		{
			Test(() => aParameter, "aParameter");
		}
	}
}
