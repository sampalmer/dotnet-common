﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using SamPalmer.Common;
using FluentAssertions;

namespace SamPalmer.Common.Tests
{
	[TestClass]
	public class IsDefaultForTypeTests
	{
		private class EqualsTracker
		{
			public override bool Equals(object obj)
			{
				EqualsCalled = true;
				return EqualityComparer<object>.Default.Equals(this, obj);
			}

			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			public bool EqualsCalled { get; private set; }
		}
		
		[TestMethod]
		public void ShouldNotCallEqualsMethod()
		{
			var tracker = new EqualsTracker();
			tracker.IsDefaultForType();
			tracker.EqualsCalled.Should().BeFalse();
		}
		
		[TestMethod]
		public void DefaultPrimitives()
		{
			default(int).IsDefaultForType().Should().BeTrue();
		}
		
		[TestMethod]
		public void NonDefaultPrimitives()
		{
			10.IsDefaultForType().Should().BeFalse();
		}
		
		[TestMethod]
		public void DefaultReferenceTypes()
		{
			object value = default(object);
			value.IsDefaultForType().Should().BeTrue();
		}
		
		[TestMethod]
		public void NonDefaultReferenceTypes()
		{
			new object().IsDefaultForType().Should().BeFalse();
		}
		
		[TestMethod]
		public void DefaultValueTypes()
		{
			default(DateTime).IsDefaultForType().Should().BeTrue();
		}
		
		[TestMethod]	
		public void NonDefaultValueTypes()
		{
			DateTime.Now.IsDefaultForType().Should().BeFalse();
		}
		
		[TestMethod]
		public void DefaultNullableTypes()
		{
			default(int?).IsDefaultForType().Should().BeTrue();
		}
		
		[TestMethod]
		public void NonDefaultNullableTypes()
		{
			var x = (int?)10; x.IsDefaultForType().Should().BeFalse(); 
		}
	}
}
