﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SamPalmer.Common.Portable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;

namespace SamPalmer.Common.Tests
{
	[TestClass]
	public class Win32ErrorTests
	{
		[TestMethod]
		public void MessagesShouldBeCorrect()
		{
			var error = new Win32Error(0);
			var message = error.GetMessage();
			message.Should().Be("The operation completed successfully." + Environment.NewLine);
		}
	}
}
