﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SamPalmer.Common.Tests.CharacterTests
{
	[TestClass]
	public class ToNumberTests
	{
		[TestMethod]
		public void ShouldWorkForZero()
		{
			Test('0', 0);
		}

		[TestMethod]
		public void ShouldWorkForNine()
		{
			Test('9', 9);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void ShouldThrowExceptionForNonNumber()
		{
			' '.ToNumber();
		}

		private static void Test(char character, int expectedValue)
		{
			var result = character.ToNumber();
			Assert.AreEqual(expectedValue, result);
		}
	}
}
