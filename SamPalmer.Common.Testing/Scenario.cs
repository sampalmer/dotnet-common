﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SamPalmer.Common.Testing
{
	[TestClass]
	public abstract class Scenario
	{
		private static HashSet<Type> initialisedScenarios = new HashSet<Type>();

		[TestInitialize]
		public void RunTests()
		{
			var scenario = GetType();

			if (!initialisedScenarios.Contains(scenario))
			{
				Given();
				When();
				initialisedScenarios.Add(scenario);
			}
		}

		protected virtual void Given()
		{
		}

		protected virtual void When()
		{
		}

		[TestCleanup]
		public virtual void Cleanup() { }
	}
}