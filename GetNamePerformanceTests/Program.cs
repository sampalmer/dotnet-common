﻿using SamPalmer.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetNamePerformanceTests
{
	class Program
	{
		private const int numberOfExecutions = 10000;

		static void Main(string[] args)
		{
			var aVariable = 0;

			var noLinqTime = TimeAverage(() => AMethodRequiringAVariableName("aVariable"), numberOfExecutions);
			var linqTime = TimeAverage(() => AMethodRequiringAVariableName(ReflectionUtilities.GetName(() => aVariable)), numberOfExecutions);

			Console.WriteLine("No LINQ time: {0}.", noLinqTime);
			Console.WriteLine("LINQ time: {0}.", linqTime);
		}

		static void AMethodRequiringAVariableName(string variableName)
		{
		}

		static TimeSpan Time(Action doSomething)
		{
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();

			var stopwatch = new Stopwatch();
			
			stopwatch.Start();
			doSomething();
			stopwatch.Stop();

			return stopwatch.Elapsed;
		}

		static TimeSpan TimeAverage(Action target, int numberOfExecutions)
		{
			var times = EnumerableUtilities.Select(() => Time(target), numberOfExecutions);
			return times.Average();
		}
	}
}
